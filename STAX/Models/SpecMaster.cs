﻿using System.Collections.Generic;

namespace STAX.Models
{
    /// <summary>
    /// Holds data related to a spec in whole
    /// </summary>
    public class SpecMaster
    {
        //Attributes
        public enum SpecStatusTypes : int
        {
            In_Spec_Build,
            Await_Development_Review,
            In_Development_Review,
            In_Development_Process,
            Complete,
            Canned
        }
        public int ID { get; set; }
        public string CrNumber { get; set; }
        public string SpecName { get; set; }
        public string SpecDesc { get; set; }
        public string SpecNotes { get; set; }

		public User CreatorUser { get; set; }

        public bool SpecHidden { get; set; }
        public SpecStatusTypes SpecStatus { get; set; }
		
        public ICollection<SpecRtpItem> SpecItems { get; set; }
        public ICollection<SpecUpdateItem> SpecUpdateItems { get; set; }

        //Constructor
        public SpecMaster()
        {

        }
    }
}
