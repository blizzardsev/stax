﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STAX.Models
{
    //Handles Spec update logging/audit trails.
    public class SpecUpdateItem
    {
        //Attrbutes
        public int ID { get; set; }
        public String Content { get; set; }
        public DateTime CreationDate { get; set; }
        public String ChangeLog { get; set; }
        public User User { get; set; }
        public SpecMaster SpecMaster { get; set; }
    }
}
