﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STAX.Models
{
    /// <summary>
    /// Handles user access to various options and abilities
    /// </summary>
    public class UserPermissions
    {
        //Attributes
        public enum CreateSpecLevels : int
        {
            Create_None = 0,
            Create_Capped = 1,
            Create_Max = 2
        }
        public enum EditSpecLevels : int
        {
            Edit_Own = 0,
            Edit_All = 1
        }

        public enum SpecialAccessLevels : int
        {
            No_Special_Access = 0,
            Special_Access = 1
        }

        public enum MessageAccessLevels : int
        {
            Read_Only = 0,
            Read_And_Send = 1
        }

        public int ID { get; set; }
        public CreateSpecLevels CreateSpecLevel { get; set; }
        public EditSpecLevels EditSpecLevel { get; set; }
        public SpecialAccessLevels SpecialAccessLevel { get; set; }
        public MessageAccessLevels MessageAccessLevel { get; set; }

        /// <summary>
        /// Update this permissions item.
        /// </summary>
        /// <param name="createSpec"></param>
        /// <param name="editSpec"></param>
        /// <param name="specialAccess"></param>
        /// <param name="messageAccess"></param>
        public void Update(int createSpec, int editSpec, int specialAccess, int messageAccess)
        {
            this.CreateSpecLevel = (CreateSpecLevels)Enum.Parse(typeof(CreateSpecLevels), createSpec.ToString());
            this.EditSpecLevel = (EditSpecLevels)Enum.Parse(typeof(EditSpecLevels), createSpec.ToString()); ;
            this.SpecialAccessLevel = (SpecialAccessLevels)Enum.Parse(typeof(SpecialAccessLevels), createSpec.ToString()); ;
            this.MessageAccessLevel = (MessageAccessLevels)Enum.Parse(typeof(MessageAccessLevels), createSpec.ToString()); ;
        }

        /// <summary>
        /// Reset this permissions item to defaults.
        /// </summary>
        public void Reset()
        {
            this.CreateSpecLevel = CreateSpecLevels.Create_None;
            this.EditSpecLevel = EditSpecLevels.Edit_Own;
            this.SpecialAccessLevel = SpecialAccessLevels.No_Special_Access;
            this.MessageAccessLevel = MessageAccessLevels.Read_Only;
        }
    }
}
