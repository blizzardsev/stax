﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STAX.Models
{
    /// <summary>
    /// Holds user preference data
    /// </summary>
    public class UserPreferences
    {
        //Attributes
        public int ID { get; set; }
        public bool SiteAnimations { get; set; }
        public bool DisableMessages { get; set; }

        //Constructor
        public UserPreferences()
        {

        }

        /// <summary>
        /// Update preferences via modal.
        /// </summary>
        /// <param name="animations"></param>
        /// <param name="systemMessages"></param>
        public void Update(bool animations, bool systemMessages)
        {
            this.SiteAnimations = animations;
            this.DisableMessages = systemMessages;
        }

        /// <summary>
        /// Reset this preferences item to default.
        /// </summary>
        public void Reset()
        {
            this.SiteAnimations = true;
            this.DisableMessages = false;
        }
    }
}
