﻿using System;
using System.Collections.Generic;

namespace STAX.Models
{
    /// <summary>
    /// Holds data for a segment of an RTP spec
    /// </summary>
    public class User
    {
        //Attributes
        public int ID { get; set; }
        public String WindowsDirRef { get; set; }
		public String FirstName { get; set; }
		public String SecondName { get; set; }
        public String ProfileImageLoc { get; set; }
        public String TeamName { get; set; }
        public bool IsActive { get; set; }
        public DateTime LastActive { get; set; }
		
		public ICollection<SpecMaster> UserSpecs { get; set; }
        public UserPreferences UserPreferences { get; set; }
        public UserPermissions UserPermissions { get; set; }

        /// <summary>
        /// Return a timestamp of how many days ago this user was last active.
        /// </summary>
        /// <returns></returns>
        public String GetLastActiveTimeStamp()
        {
            return String.Format("Last Active: {0} days ago", DateTime.Now.DayOfYear - LastActive.DayOfYear);
        }

        /// <summary>
        /// Update details via modal.
        /// </summary>
        public void Update(String firstName, String secondName, String teamName)
        {
            this.FirstName = firstName;
            this.SecondName = secondName;
            this.TeamName = teamName;
        }
    }
}
