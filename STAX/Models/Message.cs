using System;

namespace STAX.Models
{
    /// <summary>
    /// Holds data for a message
    /// </summary>
    public class Message
    {
        //Attributes
        public int ID { get; set; }
		public String MessageTitle { get; set; }
        public String MessageContent { get; set; }
        public bool MessageExpires { get; set; }
        public DateTime MessageSent { get; set; }
        public bool MessageVisible { get; set; }
        public bool SystemMessage { get; set; }

        public User SenderUser { get; set; }
        public User ReceiverUser { get; set; }
        
        //Constructor
        public Message()
        {

        }

        /// <summary>
        /// Create message via clone.
        /// </summary>
        /// <param name="existingMessage"></param>
        public Message(Message existingMessage)
        {
            MessageTitle = existingMessage.MessageTitle;
            MessageContent = existingMessage.MessageContent;
            MessageExpires = false;
            MessageSent = existingMessage.MessageSent;
            MessageVisible = false;
            SystemMessage = true;
            SenderUser = existingMessage.SenderUser;
            ReceiverUser = existingMessage.ReceiverUser;
        }

        /// <summary>
        /// Returns whether this message has expired.
        /// </summary>
        /// <returns></returns>
        public bool GetMessageHasExpired()
        {
            if (!MessageExpires) { return false; }
            else if ((DateTime.Now.DayOfYear - MessageSent.DayOfYear) > 30)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Return the age of this message in days.
        /// </summary>
        /// <returns></returns>
        public int GetMessageAge()
        {
            return (DateTime.Now - MessageSent).Days;
        }

        /// <summary>
        /// Return formatted representation of the date this message will expire.
        /// </summary>
        /// <returns></returns>
        public String GetMessageExpiryDate()
        {
            if (!MessageExpires) { return "Never"; }
            else
            {
                return MessageSent.AddDays(30).ToString("dd MM yyyy");
            }
        }
    }
}
