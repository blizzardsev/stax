﻿using System;

namespace STAX.Models
{
    /// <summary>
    /// Holds data for a segment of an RTP spec
    /// </summary>
    public class SpecRtpItem
    {
        //Attributes
        public int ID { get; set; }
        public int ItemOrderNumber { get; set; }
		public int ItemType { get; set; }
		public string ItemText { get; set; }
		public int ItemOption { get; set; }
		
		public User User { get; set; }
		public SpecMaster SpecMaster { get; set; }

        //Constructor
        public SpecRtpItem()
        {

        }

        /// <summary>
        /// Update this item.
        /// </summary>
        /// <param name="thisType"></param>
        /// <param name="thisText"></param>
        /// <param name="thisOption"></param>
        public void Update(int thisOrderNumber, int thisType, string thisText, int thisOption)
        {
            this.ItemOrderNumber = thisOrderNumber;
            this.ItemType = thisType;
            this.ItemText = thisText;
            this.ItemOption = thisOption;
        }
    }
}
