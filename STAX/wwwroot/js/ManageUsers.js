﻿//User management-specific JS
$(document).ready(function () {

});

//Update permissions item via AJAX
function SavePermissions() {
    $.ajax({
        url: '/UserPermissions/UpdatePermissions',
        type: 'POST',
        data: {
            userID: $('#perms_userID').val(),
            createSpec: $('#perms_createSpec').find('option:selected').val(),
            editSpec: $('#perms_editSpec').find('option:selected').val(),
            specialAccess: $('#perms_specialAccess').find('option:selected').val(),
            messageAccess: $('#perms_messageAccess').find('option:selected').val()
        },
        success: function (data) {
            console.log("Data retrieved successfully!");

            //Parse response data
            var responseData = $.parseJSON(data);
            if (responseData.returnSuccess == true) {
                alert("Permissions saved!");
            }
            else {
                alert("Error saving permissions! Please refresh the page and try again, or come back later.");
            }
        },
        complete: function (data) {
            console.log("Permissions data AJAX complete.");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("Permissions update failed; debug: " + thrownError);
        }
    });
}