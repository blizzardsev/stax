﻿//Handles spec index specific JS.
$(document).ready(function () {

    //Set selected options for saved items
    var allRtpSelects = $('#specmaster_specList').find('li');
    for (var i = 0; i < allRtpSelects.length; i++) {
        $(allRtpSelects[i]).find('option[js-optionVal="' + $(allRtpSelects[i]).attr('js-optionVal') + '"]').prop('selected', true);
    }

    //Set draggable list items
    $('.sortable').sortable();
    $('.sortable').disableSelection();
});

//Tag last moved spec item
$(document).on('mouseup', 'li', function () {
    $('#specmaster_specList').find('li').removeClass('lastMoved');
    $(this).addClass('lastMoved');
    SetUnsaved();
});

//Remove a spec item
$(document).on('dblclick', 'li', function () {
    if ($('#specmaster_toggleDelete').hasClass('isDeleting') == true) {
        $(this).fadeOut(50);
        if ($(this).attr('js-specItemID') == -1) {
            $(this).remove();
        }
        else {
            $(this).attr('js-isDeleted', 1);
        }
        SetUnsaved();
    }
});

//Update titles on spec item inputs
$(document).on('change', '#specmaster_specList li', function () {
    $(this).attr('title', $(this).find('input').val());
})

//Add a new spec item to the list
function AddSpecItem(itemType, objectRef) {
    if (!$(objectRef).hasClass('disabled')) {

        //Define spec items
        var specItemCollect = [
            //Condition item
            '<li class="specItem condition" js-specItemID="-1" js-specItemType="0" js-specItemText="-" js-optionVal="-1" js-isDeleted="0">'
            + '<select>'
                + '<option js-optionVal="0" selected disabled>Select a Condition</option>'
                + '<option js-optionVal="1">If</option>'
                + '<option js-optionVal="2">If Not</option>'
                + '<input type="text" placeholder="Enter a condition to check"/></li>'
            + '</select>'
            + '</li>',
            //Operator item
            '<li class="specItem operator" js-specItemID="-1" js-specItemType="1" js-specItemText="-" js-optionVal="-1" js-isDeleted="0">'
            + '<select class="centred">'
            + '<option class="centred" js-optionVal="0" selected disabled>Select an Operator</option>'
                + '<option js-optionVal="1">And</option>'
                + '<option js-optionVal="2">Or</option>'
                + '<option js-optionVal="3">Otherwise</option>'
            + '</select >'
            + '</li>',
            //Calculation item
            '<li class="specItem calculation" js-specItemID="-1" js-specItemType="2" js-specItemText="-" js-optionVal="-1" js-isDeleted="0">'
            + '<input class="centred" type="text" placeholder="Describe your Calculation">'
            + '</li>',
            //Lookup item
            '<li class="specItem lookup" js-specItemID="-1" js-specItemType="3" js-specItemText="-1" js-optionVal="-1" js-isDeleted="0">'
            + '<input class="centred" type="text" placeholder="Describe your Lookup">'
            + '</li>',
            //Action item
            '<li class="specItem action" js-specItemID="-1" js-specItemType="4" js-specItemText="-1" js-optionVal="-1" js-isDeleted="0">'
            + '<select class="centred">'
                + '<option class="centred" js-optionVal="0" selected disabled>Select an Action</option>'
                + '<option js-optionVal="1">No Quote / Decline</option>'
                + '<option js-optionVal="2">Add Item:</option>'
                + '<option js-optionVal="3">Remove Item:</option>'
                + '<option js-optionVal="4">Adjust Premium:</option>'
                + '<option js-optionVal="5">Adjust Commission:</option>'
            + '</select>'
            + '</li>',
            //Freeform item
            '<li class="specItem freeform" js-specItemID="-1" js-specItemType="5" js-specItemText="-" js-optionVal="-1" js-isDeleted="0">'
            + '<input class="centred" type="text" placeholder="Enter your Text" />'
            + '</li>',
            //Spacer item
            '<li class="specItem spacer" js-specItemID="-1" js-specItemType="6" js-specItemText="-" js-optionVal="-1" js-isDeleted="0">'
            + '</li>'
        ];

        //Add selected spec item to master list
        $('#specmaster_specList').append(specItemCollect[itemType]);
        $('#specmaster_specList').find('li').removeClass('lastAdded');
        $('#specmaster_specList').children().last().addClass('lastAdded');
        SetUnsaved();
    }
}

//Prep for save op - set parent attributes to match with item contents for easier eval on save
function SaveAll(objectRef) {
    if (!$(objectRef).hasClass('disabled')) {

        //Prepare
        var allSpecRtpItems = $('#specmaster_specList').find('li');
        var allSpecItemIDs = [];
        var allSpecItemTypes = [];
        var allSpecItemTexts = [];
        var allSpecItemOptions = [];
        var allSpecItemDeleted = [];

        for (var i = 0; i < allSpecRtpItems.length; i++) {
            var currentSpecItemType = $(allSpecRtpItems[i]).attr('js-specItemType');

            //Handle item content

                //Attempt to update item option; placeholder fallback to prevent null values
                try {
                    $(allSpecRtpItems[i]).attr('js-optionVal', $(allSpecRtpItems[i]).find('option:selected').attr('js-optionVal'));
                }
                catch {
                    $(allSpecRtpItems[i]).attr('js-optionVal', 0);
                }

                //Attempt to update item text; placeholder fallback to prevent null values
                try {
                    $(allSpecRtpItems[i]).attr('js-specItemText', $(allSpecRtpItems[i]).find('input').val());
                }
                catch {
                    $(allSpecRtpItems[i]).attr('js-specItemText', "");
                }

            //Push to arrays for AJAX
            allSpecItemIDs.push($(allSpecRtpItems[i]).attr('js-specItemID'));
            allSpecItemTypes.push($(allSpecRtpItems[i]).attr('js-specItemType'));
            allSpecItemTexts.push($(allSpecRtpItems[i]).attr('js-specItemText'));
            allSpecItemOptions.push($(allSpecRtpItems[i]).attr('js-optionVal'));
            allSpecItemDeleted.push($(allSpecRtpItems[i]).attr('js-isDeleted'));
        }

        $.ajax({
            url: '/SpecMaster/SaveAllExisting',
            type: 'POST',
            data: {
                specID: $('#specmaster_specList').attr('js-specID'),
                specItemIDs: allSpecItemIDs,
                specItemTypes: allSpecItemTypes,
                specItemTexts: allSpecItemTexts,
                specItemOptions: allSpecItemOptions,
                specItemDeleted: allSpecItemDeleted,
                specNotes: $('#specmaster_notesContent').text(),
                specStatus: $('#specmaster_statusType').find('option:selected').val(),
                specName: $('#specmaster_specName').val(),
                specDesc: $('#specmaster_specDesc').val()
            },
            success: function (data) {
                console.log("Data retrieved successfully!");

                //Parse response data, confirm save - update log items, etc
                var responseData = $.parseJSON(data);
                if (responseData.returnSuccess == true) {
                    DoConfirmPop();
                    $('#specmaster_lastSaved').text("Saved: " + responseData.returnStamp);
                    GetSpecUpdateItems();
                    $('#specmaster_statusPill').find('p').text($('#specmaster_statusType').find('option:selected').text());
                    $('#specmaster_statusPill').removeClass();
                    $('#specmaster_statusPill').addClass('status' + $('#specmaster_statusType').find('option:selected').val());
                }
                else {
                    alert("Error saving Spec! Please refresh the page and try again, or come back later.");
                    $('#specmaster_lastSaved').text("Save Failed");
                }
            },
            complete: function (data) {
                console.log("Save data AJAX complete.");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("Save failed; debug: " + thrownError);
            }
        });
    }
}

//Toggle delete mode
function ToggleDelete(objectRef) {
    if (!$(objectRef).hasClass('disabled')) {

        $(objectRef).toggleClass('isDeleting');
        if ($(objectRef).hasClass('isDeleting')) {
            $(objectRef).text('Delete Mode ON');
        }
        else {
            $(objectRef).text('Delete Mode OFF');
        }
    }
}

//Reset the builder
function Reset(objectRef) {
    if (!$(objectRef).hasClass('disabled')) {
        $('#specmaster_specList').find('li').attr('js-isDeleted', 1);
        $('#specmaster_specList').find('li').fadeOut(50);
        $('#specmaster_specList').find('li[js-specItemID="-1"]').remove();
        SetUnsaved();
    }
}

//Mark spec as unsaved
function SetUnsaved() {
    $('#specmaster_lastSaved').text("Unsaved Changes");
}

//Update the log for this spec
function GetSpecUpdateItems() {
    $.ajax({
        url: '/SpecMaster/GetSpecUpdates',
        type: 'POST',
        data: {
            specMasterID: $('#specmaster_specList').attr('js-specID')
        },

        success: function (data) {
            console.log("Data retrieved successfully!");

            //Parse response data, clear log
            var responseData = $.parseJSON(data);
            $('#specUpdates_content').find('.scrollItem').remove();

            //Refill the log
            for (var i = 0; i < responseData.updateList.length; i++) {
                $('#specUpdates_content').append(
                    '<div class="scrollItem">'
                    + '<p class="header">' + responseData.updateList[i].updateContent + '</p>'
                    + '<hr/>'
                    + '<p>' + responseData.updateList[i].updateChangeLog + '</p>'
                    + '</div>'
                );
            }
        },

        complete: function (data) {
            console.log("Update log AJAX complete.");
        },

        error: function (xhr, ajaxOptions, thrownError) {
            console.log("Update log failed; debug: " + thrownError);
        }
    });
}

//Add an update to the log for this spec
function AddSpecUpdateItem() {
    $.ajax({
        url: '/SpecMaster/AddSpecUpdate',
        type: 'POST',
        data: {
            specMasterID: $('#specmaster_specList').attr('js-specID'),
            specUpdateContent: $('#specUpdate_updateContent').val()
        },

        success: function () {
            console.log("Data retrieved successfully!");

            //Clear fields, refill the log
            $('#specUpdate_updateContent').val('');
            GetSpecUpdateItems();
        },

        complete: function () {
            console.log("Create spec update AJAX complete.");
        },

        error: function () {
            console.log("Create spec update failed; debug: " + thrownError);
        }
    });
}