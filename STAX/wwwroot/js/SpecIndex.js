﻿//Handles spec index specific JS.
$(document).ready(function () {

    $('#index_search').keyup(function () { SearchSpec(); });

    $('.index_tableDesc').click(function () {
        $(this).toggleClass('fullSize');
    })
})

//Search all specs by selected attribute and search text
function SearchSpec() {
    var attrValToSearch = $('.index_searchOption.activeSearch').attr('js-attrVal');
    var allSpecItems = $('.index_table').find('tr');
    var searchText = $("#index_search").val().toLowerCase();

    //Display/hide relevant/non-relevant items based on search criteria and this item's relevant attribute
    for (var i = 0; i < allSpecItems.length; i++) {
        if ($(allSpecItems[i]).find('td[js-attrVal="' + attrValToSearch + '"]').text().toLowerCase().indexOf(searchText) == -1) {
            $(allSpecItems[i]).find('td[js-attrVal="' + attrValToSearch + '"]').parent().fadeOut(10);
        }
        else {
            $(allSpecItems[i]).find('td[js-attrVal="' + attrValToSearch + '"]').parent().fadeIn(10);
        }
    }
}

//Switch spec attribute to search against
function SwitchSearchAttr(searchOption) {
    $('.index_searchOption').removeClass('activeSearch');
    $(searchOption).addClass('activeSearch');
    SearchSpec();
}

//Clear all search criteria
function ClearSearch() {

}

//Recolour table items in alternating fashion
function RecolourRows() {

}