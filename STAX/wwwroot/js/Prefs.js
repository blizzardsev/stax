﻿//Preferences-specific JS
$(document).ready(function () {

    //Switch/Pip toggle
    $('.modal_switch').click(function () {

        var sT = 'switchTrue';
        var sF = 'switchFalse';
        var pT = 'pipTrue';
        var pF = 'pipFalse';

        if ($(this).attr('js-switchVal') == "true") {

            //Toggle to OFF
            $(this).attr('js-switchVal', false);

            //Switch
            $(this).find('.switchLeft').removeClass(sF);
            $(this).find('.switchLeft').addClass(sT);

            $(this).find('.switchRight').removeClass(sT);
            $(this).find('.switchRight').addClass(sF);

            //Pip
            $(this).find('.switchPip').removeClass(pT);
            $(this).find('.switchPip').addClass(pF);
        }
        else {
            //Toggle to ON
            $(this).attr('js-switchVal', true);

            //Switch
            $(this).find('.switchLeft').removeClass(sT);
            $(this).find('.switchLeft').addClass(sF);

            $(this).find('.switchRight').removeClass(sF);
            $(this).find('.switchRight').addClass(sT);

            //Pip
            $(this).find('.switchPip').removeClass(pF);
            $(this).find('.switchPip').addClass(pT);
        }

        $('input[js-formMatch="' + $(this).attr('js-formMatch') + '"]').val($(this).attr('js-switchVal'));
    });
});

//Save all user preferences
function SavePrefs() {
    $.ajax({
        url: '/UserPreferences/UpdatePreferences',
        type: 'POST',
        data: {
            animations: $('#pref_Anims').attr('js-switchVal'),
            systemMessages: $('#pref_Messages').attr('js-switchVal'),
            firstName: $('#pref_firstName').val(),
            secondName: $('#pref_secondName').val(),
            teamName: $('#pref_teamName').find('option:selected').val()
        },
        success: function (data) {
            console.log("Data retrieved successfully!");

            //Parse response data
            var responseData = $.parseJSON(data);
            if (responseData.returnSuccess == true) {
                DoConfirmPop();
            }
            else {
                alert("Error saving preferences! Please refresh the page and try again, or come back later.");
            }
        },
        complete: function (data) {
            console.log("Preferences data AJAX complete.");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("Preferences update failed; debug: " + thrownError);
        }
    });
}