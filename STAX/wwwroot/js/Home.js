﻿//Home-specific JS

$(document).ready(function () {

    //Welcome bump
    setTimeout(function () {
        $('.home_banner').addClass('highlight');
    }), 350;

    setTimeout(function () {
        $('.home_banner').removeClass('highlight');
    }, 700);
});