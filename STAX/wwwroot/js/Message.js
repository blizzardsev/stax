﻿//Messages-specific JS
$(document).ready(function () {

    //Set inbox to display by default.
    $('.modal_multiPane[js-paneVal="0"]').css('display', 'block');

    //Initialize timed inbox updates - 10 second intervals
    setInterval(function () { RetrieveMessages(); }, 10000);
});

//Switch visible pane for modal dialog.
function SwitchPane(optionRef) {
    $('.modal_multiPane').css('display', 'none');
    $('.modal_multiPane[js-paneVal="' + $(optionRef).attr('js-paneVal') + '"]').css('display', 'block');
};

//Send a message
function SendMessage() {
    $.ajax({
        url: '/Message/SendMessage',
        type: 'POST',
        data: {
            title: $('#messages_titleInput').val(),
            content: $('#messages_contentInput').val(),
            expires: false,
            system: false,
            targetID: $('#messages_targetInput').find('option:selected').val()
        },
        success: function (data) {
            console.log("Data sent successfully!");

            //Parse response data, fetch new messages
            var responseData = $.parseJSON(data);
            if (responseData.returnSuccess == true) {
                //Clear inputs, update inbox
                $('#messages_titleInput').val("");
                $('#messages_contentInput').val("");
                DoConfirmPop();
                RetrieveMessages();
            }
            else {
                alert("Error sending message! Please refresh the page and try again, or come back later.");
            }
        },
        complete: function (data) {
            console.log("Message data AJAX complete.");

        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("Message send failed; debug: " + thrownError);
        }
    });
}

//Retrieve messages for this user and update the inbox
function RetrieveMessages() {
    $.ajax({
        url: '/Message/FetchMessages',
        type: 'POST',
        data: {
        },
        success: function (data) {
            console.log("Data retrieved successfully!");

            //Parse response data, clear inbox
            var responseData = $.parseJSON(data);
            $('#messages_inbox').find('.scrollItem').remove();

            if (responseData.messageList.length > 0) {
                $('#navMessages').addClass('newMessage');
                $('#navMessages').attr('title', "You have " + responseData.messageList.length + " new message(s)!");
            }
            else {
                $('#navMessages').removeClass('newMessage');
                $('#navMessages').attr('title', 'Messages');
            }

            //Create message items in inbox using response data
            for (var i = 0; i < responseData.messageList.length; i++) {
                $('#messages_inbox').append(
                    '<div class="scrollItem">'
                    + '<p class="header">Title: ' + responseData.messageList[i].messageTitle + '</p>'
                    + '<p class="header">From: ' + responseData.messageList[i].messageSender + '</p>'
                    + '<p class="header">Received: ' + responseData.messageList[i].messageDate + '</p>'
                    + '<p class="header">Expires: ' + responseData.messageList[i].messageExpires + '<p>'
                    + '<p>' + responseData.messageList[i].messageContent + '</p>'
                    + '<br>'
                    + '<div class="modal_option" title="Delete Message" js-messageID=" ' + responseData.messageList[i].messageID + '" onclick="DeleteMessage(this)">Delete</div>'
                    + '</div>'
                );
            }
            
            //Update timestamp
            $('#messages_lastUpdate').text('Inbox (Last Updated: ' + responseData.lastUpdate + ')');
        },
        complete: function (data) {
            console.log("Message data AJAX complete.");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("Message retrieve failed; debug: " + thrownError);
        }
    });
}

//Pseudo-delete message
function DeleteMessage(objectRef) {
    $.ajax({
        url: '/Message/DeleteMessage',
        type: 'POST',
        data: {
            id: $(objectRef).attr('js-messageID')
        },
        success: function (data) {
            console.log("Data retrieved successfully!");

            //Parse response data, remove deleted message visually, fetch new messages
            var responseData = $.parseJSON(data);
            if (responseData.returnSuccess == true) {
                $(objectRef).closest('scrollerItem').fadeOut(50);
                RetrieveMessages();
            }
            else {
                alert("Error deleting message! Please refresh the page and try again, or come back later.");
            }
        },
        complete: function (data) {
            console.log("Message data AJAX complete.");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("Message delete failed; debug: " + thrownError);
        }
    });
}
