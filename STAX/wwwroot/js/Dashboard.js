﻿//Dashboard-specific JS
$(document).ready(function () {

});

//Update user data via AJAX TODO
function SavePermissions(objectRef) {

    $.ajax({
        url: '/User/UpdateUserAndPerms',
        type: 'POST',
        data: {
            userID: $(objectRef).find('.userData_userID').val(),
            userFirstName: $(objectRef).find('.userData_userFirstName').val(),
            userSecondName: $(objectRef).find('.userData_userSecondName').val(),
            createSpec: $(objectRef).find('.userData_userCreatePerms').val(),
            editSpec: $(objectRef).find('.userData_userEditPerms').val(),
            specialAccess: $(objectRef).find('.userData_userSpecialPerms').val(),
            messageAccess: $(objectRef).find('.userData_userMessagePerms').val()
        },
        success: function (data) {
            console.log("Data retrieved successfully!");

            //Parse response data
            var responseData = $.parseJSON(data);
            if (responseData.returnSuccess == true) {
                DoConfirmPop();
            }
            else {
                alert("Error saving user data! Please refresh the page and try again, or come back later.");
            }
        },
        complete: function (data) {
            console.log("User data AJAX complete.");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("User update failed; debug: " + thrownError);
        }
    });
}

//Display selected tab dialog
function SwitchTab(objectRef) {
    $('.tabOption').removeClass('selected');
    $('.widget').removeClass('selected');
    $(objectRef).addClass('selected');
    $('.widget[js-optionVal="' + $(objectRef).attr('js-optionVal') + '"]').addClass('selected');
}

