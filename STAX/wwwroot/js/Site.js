﻿//Site-wide JS; globally used functions to reside here
$(document).ready(function () {

    //Site animations; pref controlled
    var global_doAnimations = $('#prefs_doAnims').val();
    if (global_doAnimations == "False") {
        $('div').css('transition', 0);
        $('input').css('transition', 0);
    }

    //Auto-hide modal dialog on click out
    $('.modal_back').click(function () {
        if (!$(this).find('.modal_fore').is(':hover')) {
            $(this).fadeOut(200);
        }  
    });

    //Fetch pending messages on each page reload
    RetrieveMessages();
});

//Display, then hide, the confirmation popup
function DoConfirmPop() {
    $('#site_confirmPopUp').fadeIn(200);
    setTimeout(function () { $('#site_confirmPopUp').fadeOut(200); }, 1500);
}

//Toggle modal display
function ToggleModal(modalVal) {
    if ($('.modal_back[js-modalVal="' + modalVal + '"]').css('display', 'none')) {
        $('.modal_back[js-modalVal="' + modalVal + '"]').fadeIn(200);
    }
    else { $('.modal_back[js-modalVal="' + modalVal + '"]').fadeOut(200); }
}

//Submit form from child
function SubmitForm(subitem) {
    $(subitem).closest('form').submit();
}