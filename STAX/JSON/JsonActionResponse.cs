﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STAX.JSON
{
    //AJAX object for generic interactions (confirmation of results)
    public class JsonActionResponse
    {
        //Attributes
        public bool returnSuccess;
        public String returnStamp;
        public String exceptionLog;

        //Constructor
        public JsonActionResponse(bool success)
        {
            this.returnSuccess = success;
            this.returnStamp = DateTime.Now.ToShortTimeString();
            this.exceptionLog = "No exception data assigned.";
        }
    }
}
