﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using STAX.Models;
using STAX.Data;
using Microsoft.EntityFrameworkCore;

namespace STAX.JSON
{
    //AJAX object for updates retrieve

    public class UpdateItemsFetchResponse
    {
        //Attributes
        public bool returnSuccess;
        public List<UpdateItem> updateList;
        internal string exceptionLog;

        //Subclass; update data - simplified for AJAX use
        public class UpdateItem
        {
            //Attributes
            public String updateContent;
            public String updateChangeLog;

            //Constructor
            public UpdateItem(String content, String changeLog)
            {
                this.updateContent = content;
                this.updateChangeLog = changeLog;
            }
        }

        //Constructor
        public UpdateItemsFetchResponse(bool success)
        {
            this.returnSuccess = success;
            this.updateList = new List<UpdateItem>();
        }
    }
}
