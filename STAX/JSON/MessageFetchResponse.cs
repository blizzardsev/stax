﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace STAX.JSON
{
    //AJAX object for message retrieve
    public class MessageFetchResponse
    {
        //Attributes
        public bool returnSuccess;
        public List<MessageItem> messageList;
        public String lastUpdate;
        internal string exceptionLog;

        //Subclass; message essential data - simplified for AJAX use
        public class MessageItem
        {
            //Attributes
            public int messageID;
            public String messageTitle;
            public String messageContent;
            public String messageSender;
            public String messageDate;
            public String messageExpires;

            //Constructor
            public MessageItem(int id, String title, String text, String sender, String date, String expires)
            {
                this.messageID = id;
                this.messageTitle = title;
                this.messageContent = text;
                this.messageSender = sender;
                this.messageDate = date;
                this.messageExpires = expires;
            }
        }

        //Constructor
        public MessageFetchResponse(bool success)
        {
            this.returnSuccess = success;
            this.messageList = new List<MessageItem>();
        }
    }
}
