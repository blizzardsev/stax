﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using STAX.Models;
using STAX.Data;
using STAX.JSON;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace STAX.Controllers
{
    public class UserPreferencesController : Controller
    {
        //Attributes
        private readonly StaxContext _context;

        //Constructor
        public UserPreferencesController(StaxContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Update user prefs; AJAX powered.
        /// </summary>
        /// <param name="animations"></param>
        /// <param name="systemMessages"></param>
        /// <param name="firstName"></param>
        /// <param name="secondName"></param>
        /// <param name="teamName"></param>
        /// <returns></returns>
        public async Task<JsonResult> UpdatePreferences(bool animations, bool systemMessages, String firstName, String secondName, String teamName)
        {
            //Initialize response object
            JsonActionResponse response;
            try
            {
                //Update prefs and user data
                response = new JsonActionResponse(true);
                UserPreferences thisUserPrefs = _context.UserCollection
                    .Include(user => user.UserPreferences)
                    .Single(user => user.WindowsDirRef == User.Identity.Name).UserPreferences;
                User thisUser = _context.UserCollection.Where(user => user.WindowsDirRef == User.Identity.Name).Single();

                thisUserPrefs.Update(animations, systemMessages);
                thisUser.Update(firstName, secondName, teamName);

                _context.Update(thisUserPrefs);
                _context.Update(thisUser);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                //Update unsuccessful, handle gracefully
                response = new JsonActionResponse(false);
                response.exceptionLog = String.Format("EXCEPTION OUTPUT: {0}", ex.ToString());
            }

            //Return
            return new JsonResult(JsonConvert.SerializeObject(response));
        }
    }
}
