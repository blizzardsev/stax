﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using STAX.Models;
using STAX.Data;

namespace STAX.Controllers
{
    public class HomeController : Controller
    {
        //Attributes
        private readonly StaxContext _context;

        //Constructor
        public HomeController(StaxContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Return site home page; create user profile if new; update user last active date.
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            try
            {
                //Update user last active
                User thisUser = _context.UserCollection.Single(user => user.WindowsDirRef == User.Identity.Name);
                thisUser.LastActive = DateTime.Today;
                _context.Update(thisUser);
                ViewBag.UserName = thisUser.FirstName + thisUser.SecondName;
            }
            catch
            {
                //User does not exist; create profile and prefs
                String userLoginSplit = User.Identity.Name.Split('\\')[1];
                _context.UserCollection.Add(
                    new User
                    {
                        WindowsDirRef = User.Identity.Name,
                        FirstName = userLoginSplit.Substring(0, 1).ToUpper(),
                        SecondName = userLoginSplit.Substring(1).ToUpper(),
                        ProfileImageLoc = "~/images/ProfilePics/newbie.png",
                        TeamName = "Unassigned",
                        IsActive = true,
                        LastActive = DateTime.Now
                    });

                //Add user preferences
                _context.UserPrefsCollection.Add(
                    new UserPreferences
                    {
                        DisableMessages = false,
                        SiteAnimations = true,
                    }
                );

                //Add user permissions
                _context.UserPermsCollection.Add(
                    new UserPermissions
                    {
                        CreateSpecLevel = UserPermissions.CreateSpecLevels.Create_None,
                        EditSpecLevel = UserPermissions.EditSpecLevels.Edit_Own,
                        SpecialAccessLevel = UserPermissions.SpecialAccessLevels.No_Special_Access,
                        MessageAccessLevel = UserPermissions.MessageAccessLevels.Read_And_Send
                    }
                );

                //Add new preferences and permissions
                _context.SaveChanges();

                //Update user with new preferences and perms
                User thisUser = _context.UserCollection.Single(user => user.WindowsDirRef == User.Identity.Name);
                thisUser.UserPreferences = _context.UserPrefsCollection.Last();
                thisUser.UserPermissions = _context.UserPermsCollection.Last();
                _context.Update(thisUser);
            }

            //Save all; return
            _context.SaveChanges();
            return View();
        }
    }
}
