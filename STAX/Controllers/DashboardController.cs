﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using STAX.Models;
using STAX.Data;
using STAX.JSON;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace STAX.Controllers
{
    public class DashboardController : Controller
    {
        //Attributes
        private readonly StaxContext _context;

        //Constructor
        public DashboardController(StaxContext context)
        {
            _context = context;
        }
    
        /// <summary>
        /// Return the dashboard.
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            return View(await _context.UserCollection.Where(user => user.WindowsDirRef == User.Identity.Name).SingleAsync());
        }
    }
}