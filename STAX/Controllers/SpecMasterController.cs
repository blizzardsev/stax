﻿using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using STAX.Models;
using STAX.Data;
using STAX.JSON;
using Microsoft.EntityFrameworkCore;
using System;
using Newtonsoft.Json;

namespace STAX.Controllers
{
    public class SpecMasterController : Controller
    {
        //Attributes
        private readonly StaxContext _context;

        //Constructor
        public SpecMasterController(StaxContext context)
        {
            _context = context;
        }

        /// <summary>
        /// All SpecMaster items listing.
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            ViewBag.AllSpecs = _context.SpecMasterCollection
                .Include(specMaster => specMaster.CreatorUser)
                .Where(specMaster => !specMaster.SpecHidden)
                .ToList();
            return View(_context.UserCollection
                .Include(user => user.UserPermissions)
                .Single(user => user.WindowsDirRef == User.Identity.Name));
        }

        /// <summary>
        /// Details for a specific SpecMaster (View a spec).
        /// </summary>
        /// <param name="thisSpecMasterID"></param>
        /// <returns></returns>
        public async Task<IActionResult> ViewSpec(int thisSpecMasterID)
        {
            //Fetch spec and associations; fetch user
            SpecMaster thisSpecMaster = await _context.SpecMasterCollection
                .Include(specMaster => specMaster.CreatorUser)
                .Include(specMaster => specMaster.SpecItems)
                .Include(specMaster => specMaster.SpecUpdateItems)
                .Where(specMaster => specMaster.ID == thisSpecMasterID)
                .SingleAsync();
            User thisUser = await _context.UserCollection.SingleAsync(user => user.WindowsDirRef == User.Identity.Name.ToUpper());

            //Return
            if (thisSpecMaster.CreatorUser.ID == thisUser.ID
                || thisUser.UserPermissions.EditSpecLevel == UserPermissions.EditSpecLevels.Edit_All)
            {
                ViewBag.CanEdit = true;
            }
            else { ViewBag.CanEdit = false; }
            return View(thisSpecMaster);
        }

        /// <summary>
        /// Create a new master spec item.
        /// </summary>
        /// <param name="thisCrNumber"></param>
        /// <param name="thisSpecName"></param>
        /// <param name="thisSpecDesk"></param>
        /// <returns></returns>
        public async Task<IActionResult> CreateSpec(string thisCrNumber, string thisSpecName, string thisSpecDesk)
        {
            //Fetch the current user
            User thisUser = await _context.UserCollection
                .Include(user => user.UserPreferences)
                .SingleAsync(user => user.WindowsDirRef == User.Identity.Name);

            //Spec creation process
            #region Spec Creation

            //Create spec
            _context.SpecMasterCollection.Add(new SpecMaster {
                CrNumber = thisCrNumber,
                SpecName = thisSpecName,
                SpecDesc = thisSpecDesk,
                CreatorUser = thisUser,
                SpecHidden = false,
                SpecStatus = SpecMaster.SpecStatusTypes.In_Spec_Build
            });

            //Create spec update for creation process
            await _context.SaveChangesAsync();
            SpecMaster newlyAddedSpec = await _context.SpecMasterCollection.LastAsync();
            await _context.SpecUpdateCollection.AddAsync(new SpecUpdateItem
            {
                Content = String.Format("Spec No #{0} was created on {1}, at {2} by {3}:",
                    newlyAddedSpec.ID,
                    DateTime.Now.ToShortDateString(),
                    DateTime.Now.ToShortTimeString(),
                    String.Format("{0} {1}", thisUser.FirstName, thisUser.SecondName)),
                ChangeLog = "Spec was created.",
                CreationDate = DateTime.Now,
                User = thisUser,
                SpecMaster = newlyAddedSpec
            });

            #endregion

            //Send confirmation message, if enabled by user prefs
            if (!thisUser.UserPreferences.DisableMessages)
            {
                _context.MessageCollection.Add(new Message
                {
                    MessageTitle = "Success! Your new Spec is ready.",
                    MessageContent = "Congratulations - be sure to save your changes regularly!",
                    MessageExpires = true,
                    MessageSent = DateTime.Now,
                    MessageVisible = true,
                    SystemMessage = true,
                    SenderUser = await _context.UserCollection.FindAsync(1),
                    ReceiverUser = await _context.UserCollection.Where(user => user.WindowsDirRef == User.Identity.Name).SingleAsync()
                });
            }

            //Return
            await _context.SaveChangesAsync();
            return RedirectToAction("ViewSpec", "SpecMaster", new { thisSpecMasterID = _context.SpecMasterCollection.Last().ID });
        }

        /// <summary>
        /// Create/Update and save all spec RTP items iteratively; AJAX powered.
        /// </summary>
        /// <param name="specID"></param>
        /// <param name="specItemIDs"></param>
        /// <param name="specItemTypes"></param>
        /// <param name="specItemTexts"></param>
        /// <param name="specItemOptions"></param>
        /// <param name="specItemDeleted"></param>
        /// <param name="specNotes"></param>
        /// <returns></returns>
        public async Task<JsonResult> SaveAllExisting(int specID,
            int[] specItemIDs,
            int[]specItemTypes,
            String[] specItemTexts,
            int[] specItemOptions,
            int[] specItemDeleted,
            String specNotes,
            int specStatus,
            String specName,
            String specDesc)
        {
            //Initialize response object
            JsonActionResponse response;
            try
            {
                response = new JsonActionResponse(true);

                //Changelog statistics
                int numberItemsAdded = 0;
                int numberItemsRemoved = 0;
                int numberItemsUpdated = 0;

                //Create/Update/Delete RTP items
                for (int i = 0; i < specItemIDs.Length; i++)
                {
                    //New RtpItems initialized with ID attribute -1
                    bool isNewItem = specItemIDs[i] == -1;

                    //Item does not exist - Create new item
                    #region New RTP item

                    if (isNewItem)
                    {
                        //Item does not already exist, create with params and add to database
                        await _context.AddAsync(new SpecRtpItem
                        {
                            ItemOrderNumber = i,
                            ItemType = specItemTypes[i],
                            ItemText = specItemTexts[i],
                            ItemOption = specItemOptions[i],
                            User = await _context.UserCollection.Where(user => user.WindowsDirRef == User.Identity.Name).SingleAsync(),
                            SpecMaster = await _context.SpecMasterCollection.FindAsync(specID)
                        });
                        numberItemsAdded++;
                    }

                    #endregion

                    //Item does exist - Update / Delete existing item
                    #region Existing RTP item

                    else
                    {
                        //Item already exists; update or delete accordingly
                        if (specItemDeleted[i] == 1)
                        {
                            //Delete
                            _context.SpecRtpItemCollection.Remove(_context.SpecRtpItemCollection.Find(specItemIDs[i]));
                            numberItemsRemoved++;
                        }
                        else
                        {
                            //Update
                            SpecRtpItem thisSpecRtpItem = await _context.SpecRtpItemCollection.FindAsync(specItemIDs[i]);
                            thisSpecRtpItem.Update(i, specItemTypes[i], specItemTexts[i], specItemOptions[i]);
                            _context.SpecRtpItemCollection.Update(thisSpecRtpItem);

                            //Increment updated count only if item has changed
                            if (thisSpecRtpItem.ItemOrderNumber != i
                                    || thisSpecRtpItem.ItemType != specItemTypes[i]
                                    || thisSpecRtpItem.ItemText != specItemTexts[i]
                                    || thisSpecRtpItem.ItemOption != specItemOptions[i])
                            {
                                numberItemsUpdated++;
                            }           
                        }
                    }

                    #endregion
                }

                #region SpecMaster update

                //Fetch SpecMaster and current User
                SpecMaster thisSpecMaster = await _context.SpecMasterCollection.FindAsync(specID);
                User thisUser = await _context.UserCollection.Where(user => user.WindowsDirRef == User.Identity.Name).SingleAsync();

                //Build changelog
                #region Changelog

                String changeLog = "";
                String[] statusRef =
                {
                    "Under Construction",
                    "Awaiting Review",
                    "Under Review",
                    "In Development",
                    "Completed",
                    "Canned"
                };

                //Determine changelog content

                if ((int)thisSpecMaster.SpecStatus != specStatus)
                {
                    changeLog += String.Format("Spec status updated: {0}</br>", statusRef[specStatus]);
                }
                if (numberItemsAdded > 0)
                {
                    changeLog += String.Format("Spec items added: {0}</br>", numberItemsAdded);
                }
                if (numberItemsRemoved > 0)
                {
                    changeLog += String.Format("Spec items removed: {0}</br>", numberItemsRemoved);
                }
                if (numberItemsUpdated > 0)
                {
                    changeLog += String.Format("Spec items updated: {0}</br>", numberItemsUpdated);
                }
                if (thisSpecMaster.SpecNotes != specNotes)
                {
                    changeLog += "Spec notes updated.</br>";
                }
                if (thisSpecMaster.SpecName != specName)
                {
                    changeLog += "Spec name updated.</br>";
                }
                if (thisSpecMaster.SpecDesc != specDesc)
                {
                    changeLog += "Spec description updated.</br>";
                }

                if (changeLog == "")
                {
                    changeLog = "No changes were detected.</br>";
                }

                #endregion

                //Add spec update item
                await _context.SpecUpdateCollection.AddAsync(new SpecUpdateItem {
                    Content = String.Format("Spec was updated on {0}, at {1} by {2}:",
                        DateTime.Now.ToShortDateString(),
                        DateTime.Now.ToShortTimeString(),
                        String.Format("{0} {1}", thisUser.FirstName, thisUser.SecondName)
                    ),
                    CreationDate = DateTime.Now,
                    ChangeLog = changeLog,
                    User = thisUser,
                    SpecMaster = thisSpecMaster
                });

                //Update spec notes, spec status, spec name and spec desc
                thisSpecMaster.SpecNotes = specNotes;
                thisSpecMaster.SpecStatus = (SpecMaster.SpecStatusTypes)Enum.Parse(typeof(SpecMaster.SpecStatusTypes), specStatus.ToString());
                thisSpecMaster.SpecName = specName;
                thisSpecMaster.SpecDesc = specDesc;
                _context.Update(thisSpecMaster);

                #endregion

                //Finally save all changes
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                //Update unsuccessful, handle gracefully
                response = new JsonActionResponse(false);
                response.exceptionLog = String.Format("EXCEPTION OUTPUT: {0}", ex.ToString());
            }

            //Return
            return new JsonResult(JsonConvert.SerializeObject(response));
        }

        /// <summary>
        /// Fetch all spec update items; AJAX powered.
        /// </summary>
        /// <param name="specMasterID"></param>
        /// <returns></returns>
        public async Task<JsonResult> GetSpecUpdates(int specMasterID)
        {
            //Initialize response object
            UpdateItemsFetchResponse response;
            try
            {
                response = new UpdateItemsFetchResponse(true);

                List<SpecUpdateItem> allSpecUpdates = await _context.SpecUpdateCollection
                    .Include(specUpdateItem => specUpdateItem.SpecMaster)
                    .Where(specUpdateItem => specUpdateItem.SpecMaster.ID == specMasterID)
                    .OrderByDescending(specUpdateItem => specUpdateItem.CreationDate)
                    .ToListAsync();
                foreach (SpecUpdateItem thisSpecUpdateItem in allSpecUpdates)
                {
                    response.updateList.Add(new UpdateItemsFetchResponse.UpdateItem(
                        thisSpecUpdateItem.Content,
                        thisSpecUpdateItem.ChangeLog));
                }
            }
            catch (Exception ex)
            {
                //Fetch unsuccessful, handle gracefully
                response = new UpdateItemsFetchResponse(false);
                response.exceptionLog = String.Format("EXCEPTION OUTPUT: {0}", ex.ToString());
            }

            return new JsonResult(JsonConvert.SerializeObject(response));
        }

        /// <summary>
        /// Add a spec update item; AJAX powered.
        /// </summary>
        /// <returns></returns>
        public async Task<JsonResult> AddSpecUpdate(int specMasterID, String specUpdateContent)
        {
            //Initialize response object
            JsonActionResponse response;
            try
            {
                response = new JsonActionResponse(true);

                //Get the Spec Master, include the creator
                SpecMaster thisSpecMaster = await _context.SpecMasterCollection
                    .Include(specMaster => specMaster.CreatorUser)
                    .Where(specMaster => specMaster.ID == specMasterID)
                    .SingleAsync();

                //Add the user's update
                User thisUser = await _context.UserCollection.SingleAsync(user => user.WindowsDirRef == User.Identity.Name);
                await _context.SpecUpdateCollection.AddAsync(new SpecUpdateItem
                {
                    Content = String.Format("{0} added an update, on {1} at {2}:",
                    String.Format("{0} {1}", thisUser.FirstName, thisUser.SecondName),
                    DateTime.Now.ToShortDateString(),
                    DateTime.Now.ToShortTimeString()),
                    ChangeLog = specUpdateContent,
                    CreationDate = DateTime.Now,
                    User = thisUser,
                    SpecMaster = thisSpecMaster
                });

                //Message the spec owner, if not posting own update
                if (thisSpecMaster.CreatorUser != thisUser)
                {
                    _context.MessageCollection.Add(new Message
                    {
                        MessageTitle = "A user has posted a manual update to your Spec!",
                        MessageContent = String.Format("{0} {1} posted an update to {2} ({3}): {4}.",
                            thisUser.FirstName,
                            thisUser.SecondName,
                            thisSpecMaster.CrNumber,
                            thisSpecMaster.SpecName,
                            specUpdateContent),
                        MessageExpires = false,
                        MessageSent = DateTime.Now,
                        MessageVisible = true,
                        SystemMessage = true,
                        SenderUser = await _context.UserCollection.FindAsync(1),
                        ReceiverUser = thisSpecMaster.CreatorUser
                    });
                }

                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                //Fetch unsuccessful, handle gracefully
                response = new JsonActionResponse(false);
                response.exceptionLog = String.Format("EXCEPTION OUTPUT: {0}", ex.ToString());
            }

            return new JsonResult(JsonConvert.SerializeObject(response));
        }
    }
}
