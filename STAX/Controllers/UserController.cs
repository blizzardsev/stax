﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using STAX.Models;
using STAX.Data;
using STAX.JSON;
using Newtonsoft.Json;

namespace STAX.Controllers
{
    public class UserController : Controller
    {
        //Attributes
        private readonly StaxContext _context;

        //Constructor
        public UserController(StaxContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Return user creation page.
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Return dedicated user profile page.
        /// </summary>
        /// <returns></returns>
        public IActionResult Profile()
        {
            return View();
        }

        /// <summary>
        /// Return user management page.
        /// </summary>
        /// <returns></returns>
        public IActionResult Manage()
        {
            ViewBag.AllUsers = _context.UserCollection
                .Include(user => user.UserPermissions)
                .ToList();
            return View();
        }

        /// <summary>
        /// Create a new user.
        /// </summary>
        /// <param name="thisFirstName"></param>
        /// <param name="thisSecondName"></param>
        /// <param name="thisProfileImageLoc"></param>
        /// <param name="thisTeamName"></param>
        /// <returns></returns>
        public async Task CreateUser(String thisFirstName,
            String thisSecondName,
            String thisProfileImageLoc,
            String thisTeamName)
        {
            await _context.AddAsync(new User {
                FirstName = thisFirstName,
                SecondName = thisSecondName,
                ProfileImageLoc = thisProfileImageLoc,
                TeamName = thisTeamName,
                IsActive = true
            });

            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Edit the existing user's details.
        /// </summary>
        /// <param name="thisFirstName"></param>
        /// <param name="thisSecondName"></param>
        /// <param name="thisProfileImageLoc"></param>
        /// <param name="thisTeamName"></param>
        /// <returns></returns>
        public async Task UpdateUser(String thisFirstName,
            String thisSecondName,
            String thisProfileImageLoc,
            String thisTeamName)
        {
            //Update the target user with given params
            User thisUser = _context.UserCollection.Single(user => user.WindowsDirRef == "");
            thisUser.FirstName = thisFirstName;
            thisUser.SecondName = thisSecondName;
            thisUser.ProfileImageLoc = thisProfileImageLoc;
            thisUser.TeamName = thisTeamName;

            _context.Update(thisUser);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Update an existing user and their permissions; 
        /// </summary>
        /// <param name="thisUserID"></param>
        /// <param name="thisFirstName"></param>
        /// <param name="thisSecondName"></param>
        /// <param name="thisCreatePerms"></param>
        /// <param name="thisEditPerms"></param>
        /// <param name="thisSpecialPerms"></param>
        /// <param name="thisMessagePerms"></param>
        /// <returns></returns>
        public async Task<JsonResult> UpdateUserAndPerms(
            int thisUserID,
            String thisFirstName,
            String thisSecondName,
            int thisCreatePerms,
            int thisEditPerms,
            int thisSpecialPerms,
            int thisMessagePerms)
        {
            //Initialize response object
            JsonActionResponse response;
            try
            {
                //Update referenced user with form data
                User thisUser = await _context.UserCollection
                    .Include(user => user.UserPermissions)
                    .SingleAsync(user => user.ID == thisUserID);
                thisUser.FirstName = thisFirstName;
                thisUser.SecondName = thisSecondName;

                //Update referenced user permissions with form data
                UserPermissions thisUserPerms = thisUser.UserPermissions;
                thisUserPerms.Update(thisCreatePerms, thisEditPerms, thisSpecialPerms, thisMessagePerms);
                _context.Update(thisUser);
                _context.Update(thisUserPerms);

                //Set response and save changes
                response = new JsonActionResponse(true);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                //Send unsuccessful, handle gracefully
                response = new JsonActionResponse(false);
                response.exceptionLog = String.Format("EXCEPTION OUTPUT: {0}", ex.ToString());
            }

            //Return
            return new JsonResult(JsonConvert.SerializeObject(response));
        }
    }
}
