﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using STAX.Models;
using STAX.Data;
using STAX.JSON;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace STAX.Controllers
{
    public class UserPermissionsController : Controller
    {
        //Attributes
        private readonly StaxContext _context;

        //Constructor
        public UserPermissionsController(StaxContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Update user perms; AJAX powered.
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="createSpec"></param>
        /// <param name="editSpec"></param>
        /// <param name="specialAccess"></param>
        /// <param name="messageAccess"></param>
        /// <returns></returns>
        public async Task<JsonResult> UpdatePermissions(int userID, int createSpec, int editSpec, int specialAccess, int messageAccess)
        {
            //Initialize response object
            JsonActionResponse response;
            try
            {
                //Update permissions
                response = new JsonActionResponse(true);
                UserPermissions thisUserPerms = _context.UserCollection
                    .Include(user => user.UserPermissions)
                    .Single(user => user.ID == userID).UserPermissions;
                thisUserPerms.Update(createSpec, editSpec, specialAccess, messageAccess);

                _context.Update(thisUserPerms);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                //Update failed; handle gracefully
                response = new JsonActionResponse(false);
                response.exceptionLog = String.Format("EXCEPTION OUTPUT: {0}", ex.ToString());
            }

            //Return
            return new JsonResult(JsonConvert.SerializeObject(response));
        }
    }
}
