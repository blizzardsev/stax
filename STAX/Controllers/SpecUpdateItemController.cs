﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using STAX.Data;
using STAX.Models;

namespace STAX.Controllers
{
    //Handles SpecUpdateItem related functionality outside of SpecMaster scope
    public class SpecUpdateItemController : Controller
    {
        //Attributes
        private readonly StaxContext _context;

        //Constructor
        public SpecUpdateItemController(StaxContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Add a manual user update.
        /// </summary>
        /// <param name="thisUpdateSpecID"></param>
        /// <param name="thisUpdateContent"></param>
        /// <returns></returns>
        public async Task<IActionResult> CreateUpdate(int thisUpdateSpecID, String thisUpdateContent)
        {
            //Get the Spec Master, include the creator
            SpecMaster thisSpecMaster = await _context.SpecMasterCollection
                .Include(specMaster => specMaster.CreatorUser)
                .Where(specMaster => specMaster.ID == thisUpdateSpecID)
                .SingleAsync();

            //Add the user's update
            User thisUser = await _context.UserCollection.SingleAsync(user => user.WindowsDirRef == User.Identity.Name);
            await _context.SpecUpdateCollection.AddAsync(new SpecUpdateItem
            {
                Content = String.Format("{0} added an update, on {1} at {2}:",
                String.Format("{0} {1}", thisUser.FirstName, thisUser.SecondName),
                DateTime.Now.ToShortDateString(),
                DateTime.Now.ToShortTimeString()),
                ChangeLog = thisUpdateContent,
                CreationDate = DateTime.Now,
                User = thisUser,
                SpecMaster = thisSpecMaster
            });

            //Message the spec owner, if not posting own update
            if (thisSpecMaster.CreatorUser != thisUser)
            {
                _context.MessageCollection.Add(new Message
                {
                    MessageTitle = "A user has posted a manual update to your Spec!",
                    MessageContent = String.Format("{0} {1} posted an update to {2} ({3}): {4}.",
                        thisUser.FirstName,
                        thisUser.SecondName,
                        thisSpecMaster.CrNumber,
                        thisSpecMaster.SpecName,
                        thisUpdateContent),
                    MessageExpires = false,
                    MessageSent = DateTime.Now,
                    MessageVisible = true,
                    SystemMessage = true,
                    SenderUser = await _context.UserCollection.FindAsync(1),
                    ReceiverUser = thisSpecMaster.CreatorUser
                });
            }

            //Return
            await _context.SaveChangesAsync();
            return RedirectToAction("ViewSpec", "SpecMaster", new { thisSpecMasterID = thisUpdateSpecID });
        }
    }
}
