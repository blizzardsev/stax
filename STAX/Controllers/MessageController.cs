﻿using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using STAX.Models;
using STAX.Data;
using STAX.JSON;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;

namespace STAX.Controllers
{
    public class MessageController : Controller
    {
        //Attributes
        private readonly StaxContext _context;

        //Constructor
        public MessageController(StaxContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Create a message, add to DB
        /// </summary>
        /// <param name="thisMessageTitle"></param>
        /// <param name="thisMessageContent"></param>
        /// <param name="thisMessageExpires"></param>
        /// <param name="thisUserID"></param>
        /// <param name="thisTargetUserID"></param>
        /// <returns></returns>
        public async Task<JsonResult> SendMessage(String title,
            String content,
            bool expires,
            bool system,
            int targetID)
        {
            //Initialize response object
            JsonActionResponse response;
            try
            {
                //Create messages with params and add to database
                await _context.MessageCollection.AddAsync(new Message {
                    MessageTitle = title,
                    MessageContent = content,
                    MessageExpires = expires,
                    MessageSent = DateTime.Now,
                    MessageVisible = true,
                    SystemMessage = system,
                    SenderUser = await _context.UserCollection.SingleAsync(user => user.WindowsDirRef == User.Identity.Name),
                    ReceiverUser = await _context.UserCollection.FindAsync(targetID)
                });

                //Set response and save changes
                response = new JsonActionResponse(true);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                //Send unsuccessful, handle gracefully
                response = new JsonActionResponse(false);
                response.exceptionLog = String.Format("EXCEPTION OUTPUT: {0}", ex.ToString());
            }

            //Return
            return new JsonResult(JsonConvert.SerializeObject(response));
        }

        /// <summary>
        /// Fetch all messages for this user; AJAX powered.
        /// </summary>
        /// <returns></returns>
        public async Task<JsonResult> FetchMessages()
        {
            //Initialize response object
            MessageFetchResponse response;
            try
            {
                //Fetch messages from database, assign to JSON response
                response = new MessageFetchResponse(true);
                List<Message> userMessages = await _context.MessageCollection
                    .Include(message => message.ReceiverUser)
                    .Include(message => message.SenderUser)
                    .Where(message => message.ReceiverUser.WindowsDirRef == User.Identity.Name && message.MessageVisible)
                    .ToListAsync();
                foreach(Message thisMessage in userMessages)
                {
                    //Only add messages sent to this user and not marked as removed
                    MessageFetchResponse.MessageItem messageItem = new MessageFetchResponse.MessageItem(
                        thisMessage.ID,
                        thisMessage.MessageTitle,
                        thisMessage.MessageContent,
                        thisMessage.SystemMessage ? "System" : String.Format("{0}{1}", thisMessage.SenderUser.FirstName, thisMessage.SenderUser.SecondName),
                        String.Format("{0} at {1}", thisMessage.MessageSent.ToShortDateString(), thisMessage.MessageSent.ToShortTimeString()),
                        thisMessage.GetMessageExpiryDate());
                    response.messageList.Add(messageItem);
                }

                //Update response timestamp
                response.lastUpdate = DateTime.Now.ToShortTimeString();
            }
            catch (Exception ex)
            {
                //Fetch unsuccessful, handle gracefully
                response = new MessageFetchResponse(false);
                response.exceptionLog = String.Format("EXCEPTION OUTPUT: {0}", ex.ToString());
            }

            //Return
            return new JsonResult(JsonConvert.SerializeObject(response));
        }

        //Pseudo-delete a specific message
        public async Task<JsonResult> DeleteMessage(int id)
        {
            //Initialize response object
            JsonActionResponse response;
            try
            {
                //Remove message from the database
                response = new JsonActionResponse(true);
                Message thisMessage = await _context.MessageCollection.FindAsync(id);
                thisMessage.MessageVisible = false;
                _context.Update(thisMessage);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                //Removal unsuccessful, handle gracefully
                response = new JsonActionResponse(false);
                response.exceptionLog = String.Format("EXCEPTION OUTPUT: {0}", ex.ToString());
            }
            return new JsonResult(JsonConvert.SerializeObject(response));
        }
    }
}
