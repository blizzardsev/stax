#pragma checksum "D:\Repository\stax\STAX\Views\Shared\Components\UserData\Default.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "aadf6bf00648b569d0458be604d3936712aa0660"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared_Components_UserData_Default), @"mvc.1.0.view", @"/Views/Shared/Components/UserData/Default.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Shared/Components/UserData/Default.cshtml", typeof(AspNetCore.Views_Shared_Components_UserData_Default))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Repository\stax\STAX\Views\_ViewImports.cshtml"
using STAX;

#line default
#line hidden
#line 2 "D:\Repository\stax\STAX\Views\_ViewImports.cshtml"
using STAX.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"aadf6bf00648b569d0458be604d3936712aa0660", @"/Views/Shared/Components/UserData/Default.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"134a1cb67cbd98bb021e953da43880b714c790f8", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared_Components_UserData_Default : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<STAX.Models.User>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(25, 50, true);
            WriteLiteral("\r\n<!--Render username on menu bar-->\r\n<p>Welcome, ");
            EndContext();
            BeginContext(77, 53, false);
#line 4 "D:\Repository\stax\STAX\Views\Shared\Components\UserData\Default.cshtml"
        Write(Model.FirstName.Length > 1 ? Model.FirstName : "User");

#line default
#line hidden
            EndContext();
            BeginContext(131, 85, true);
            WriteLiteral("</p>\r\n\r\n<!--Global user preferences-->\r\n<input id=\"globalPrefs_doAnims\" type=\"hidden\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 216, "\"", 272, 1);
#line 7 "D:\Repository\stax\STAX\Views\Shared\Components\UserData\Default.cshtml"
WriteAttributeValue("", 224, Model.UserPreferences.SiteAnimations.ToString(), 224, 48, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(273, 87, true);
            WriteLiteral(" />\r\n\r\n<!--Global user permissions-->\r\n<input id=\"globalPerms_createSpec\" type=\"hidden\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 360, "\"", 406, 1);
#line 10 "D:\Repository\stax\STAX\Views\Shared\Components\UserData\Default.cshtml"
WriteAttributeValue("", 368, Model.UserPermissions.CreateSpecLevel, 368, 38, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(407, 51, true);
            WriteLiteral(" />\r\n<input id=\"globalPerms_editSpec\" type=\"hidden\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 458, "\"", 502, 1);
#line 11 "D:\Repository\stax\STAX\Views\Shared\Components\UserData\Default.cshtml"
WriteAttributeValue("", 466, Model.UserPermissions.EditSpecLevel, 466, 36, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(503, 56, true);
            WriteLiteral(" />\r\n<input id=\"globalPerms_specialAccess\" type=\"hidden\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 559, "\"", 608, 1);
#line 12 "D:\Repository\stax\STAX\Views\Shared\Components\UserData\Default.cshtml"
WriteAttributeValue("", 567, Model.UserPermissions.SpecialAccessLevel, 567, 41, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(609, 56, true);
            WriteLiteral(" />\r\n<input id=\"globalPerms_messageAccess\" type=\"hidden\"");
            EndContext();
            BeginWriteAttribute("value", " value=\"", 665, "\"", 714, 1);
#line 13 "D:\Repository\stax\STAX\Views\Shared\Components\UserData\Default.cshtml"
WriteAttributeValue("", 673, Model.UserPermissions.MessageAccessLevel, 673, 41, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(715, 5, true);
            WriteLiteral(" />\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<STAX.Models.User> Html { get; private set; }
    }
}
#pragma warning restore 1591
