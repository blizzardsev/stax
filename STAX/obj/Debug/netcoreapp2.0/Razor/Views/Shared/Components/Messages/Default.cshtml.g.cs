#pragma checksum "D:\Repository\stax\STAX\Views\Shared\Components\Messages\Default.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b6e19cf5555de271c3caf05d33bcaaad21fd001c"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared_Components_Messages_Default), @"mvc.1.0.view", @"/Views/Shared/Components/Messages/Default.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Shared/Components/Messages/Default.cshtml", typeof(AspNetCore.Views_Shared_Components_Messages_Default))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Repository\stax\STAX\Views\_ViewImports.cshtml"
using STAX;

#line default
#line hidden
#line 2 "D:\Repository\stax\STAX\Views\_ViewImports.cshtml"
using STAX.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b6e19cf5555de271c3caf05d33bcaaad21fd001c", @"/Views/Shared/Components/Messages/Default.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"134a1cb67cbd98bb021e953da43880b714c790f8", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared_Components_Messages_Default : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<STAX.Models.User>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Message", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Create", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(25, 15, true);
            WriteLiteral("\r\n<!--VARS-->\r\n");
            EndContext();
#line 4 "D:\Repository\stax\STAX\Views\Shared\Components\Messages\Default.cshtml"
  
    List<STAX.Models.Message> allMessages = ViewBag.AllMessages;
    List<STAX.Models.User> allUsers = ViewBag.AllUsers;

#line default
#line hidden
            BeginContext(170, 246, true);
            WriteLiteral("\r\n<!--MESSAGES-->\r\n\r\n<!--Main modal content-->\r\n<div class=\"modal_fore\">\r\n    <p class=\"mainHeader\">Messages</p>\r\n\r\n    <!--Inbox Listing-->\r\n    <div class=\"modal_multiPane\" js-paneVal=\"0\">\r\n        <p class=\"subHeader\" id=\"messages_lastUpdate\">");
            EndContext();
            BeginContext(417, 76, false);
#line 17 "D:\Repository\stax\STAX\Views\Shared\Components\Messages\Default.cshtml"
                                                 Write(String.Format("Inbox (Last Updated: {0})", DateTime.Now.ToShortTimeString()));

#line default
#line hidden
            EndContext();
            BeginContext(493, 64, true);
            WriteLiteral("</p>\r\n        <div id=\"messages_inbox\" class=\"modal_scroller\">\r\n");
            EndContext();
#line 19 "D:\Repository\stax\STAX\Views\Shared\Components\Messages\Default.cshtml"
             foreach (STAX.Models.Message thisMessage in allMessages)
            {
                var senderText = thisMessage.SystemMessage ? "From: System" : String.Format("{0} {1}", thisMessage.SenderUser.FirstName, thisMessage.SenderUser.SecondName);

#line default
#line hidden
            BeginContext(817, 87, true);
            WriteLiteral("                <div class=\"scrollItem\">\r\n                    <p class=\"header\">Title: ");
            EndContext();
            BeginContext(905, 24, false);
#line 23 "D:\Repository\stax\STAX\Views\Shared\Components\Messages\Default.cshtml"
                                        Write(thisMessage.MessageTitle);

#line default
#line hidden
            EndContext();
            BeginContext(929, 50, true);
            WriteLiteral("</p>\r\n                    <p class=\"header\">From: ");
            EndContext();
            BeginContext(980, 10, false);
#line 24 "D:\Repository\stax\STAX\Views\Shared\Components\Messages\Default.cshtml"
                                       Write(senderText);

#line default
#line hidden
            EndContext();
            BeginContext(990, 54, true);
            WriteLiteral("</p>\r\n                    <p class=\"header\">Received: ");
            EndContext();
            BeginContext(1045, 117, false);
#line 25 "D:\Repository\stax\STAX\Views\Shared\Components\Messages\Default.cshtml"
                                           Write(String.Format("{0} at {1}", thisMessage.MessageSent.ToShortDateString(), thisMessage.MessageSent.ToShortTimeString()));

#line default
#line hidden
            EndContext();
            BeginContext(1162, 53, true);
            WriteLiteral("</p>\r\n                    <p class=\"header\">Expires: ");
            EndContext();
            BeginContext(1216, 34, false);
#line 26 "D:\Repository\stax\STAX\Views\Shared\Components\Messages\Default.cshtml"
                                          Write(thisMessage.GetMessageExpiryDate());

#line default
#line hidden
            EndContext();
            BeginContext(1250, 29, true);
            WriteLiteral("</p>\r\n                    <p>");
            EndContext();
            BeginContext(1280, 26, false);
#line 27 "D:\Repository\stax\STAX\Views\Shared\Components\Messages\Default.cshtml"
                  Write(thisMessage.MessageContent);

#line default
#line hidden
            EndContext();
            BeginContext(1306, 100, true);
            WriteLiteral("</p>\r\n                    <br>\r\n                    <div class=\"modal_option\" title=\"Delete Message\"");
            EndContext();
            BeginWriteAttribute("js-messageID", " js-messageID=\"", 1406, "\"", 1436, 1);
#line 29 "D:\Repository\stax\STAX\Views\Shared\Components\Messages\Default.cshtml"
WriteAttributeValue("", 1421, thisMessage.ID, 1421, 15, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1437, 69, true);
            WriteLiteral(" onclick=\"DeleteMessage(this)\">Delete</div>\r\n                </div>\r\n");
            EndContext();
#line 31 "D:\Repository\stax\STAX\Views\Shared\Components\Messages\Default.cshtml"
            }

#line default
#line hidden
            BeginContext(1521, 218, true);
            WriteLiteral("        </div>\r\n        <div class=\"modal_option\" onclick=\"RetrieveMessages()\" title=\"Refresh Inbox\">Refresh</div>\r\n    </div>\r\n\r\n    <!--Message Composure-->\r\n    <div class=\"modal_multiPane\" js-paneVal=\"1\">\r\n        ");
            EndContext();
            BeginContext(1739, 873, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a3680a58ba3e4d7086d6e8030dece93c", async() => {
                BeginContext(1790, 160, true);
                WriteLiteral("\r\n            <p class=\"subHeader\">Compose</p>\r\n            <br>\r\n            <p class=\"subHeader\">Send To</p>\r\n            <select id=\"messages_targetInput\">\r\n");
                EndContext();
#line 43 "D:\Repository\stax\STAX\Views\Shared\Components\Messages\Default.cshtml"
                 foreach (STAX.Models.User thisUser in allUsers)
                {

#line default
#line hidden
                BeginContext(2035, 20, true);
                WriteLiteral("                    ");
                EndContext();
                BeginContext(2055, 104, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "10f92b954f4d4256ac5b8b9781a36aa1", async() => {
                    BeginContext(2085, 65, false);
#line 45 "D:\Repository\stax\STAX\Views\Shared\Components\Messages\Default.cshtml"
                                            Write(String.Format("{0} {1}", thisUser.FirstName, thisUser.SecondName));

#line default
#line hidden
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                BeginWriteTagHelperAttribute();
#line 45 "D:\Repository\stax\STAX\Views\Shared\Components\Messages\Default.cshtml"
                       WriteLiteral(thisUser.ID);

#line default
#line hidden
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = __tagHelperStringValueBuffer;
                __tagHelperExecutionContext.AddTagHelperAttribute("value", __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(2159, 2, true);
                WriteLiteral("\r\n");
                EndContext();
#line 46 "D:\Repository\stax\STAX\Views\Shared\Components\Messages\Default.cshtml"
                }

#line default
#line hidden
                BeginContext(2180, 425, true);
                WriteLiteral(@"            </select>
            <p class=""subHeader"">Message Title</p>
            <input id=""messages_titleInput"" type=""text"" placeholder=""Your message title."" />

            <p class=""subHeader"">Message Content</p>
            <input id=""messages_contentInput"" type=""text"" placeholder=""Your message text."" />

            <div class=""modal_option"" title=""Send Message"" onclick=""SendMessage()"">Send</div>
        ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2612, 158, true);
            WriteLiteral("\r\n    </div>\r\n\r\n    <!--Main options-->\r\n    <br>\r\n    <div class=\"modal_option\" js-paneVal=\"0\" onclick=\"SwitchPane(this)\" title=\"View Messages\">Inbox</div>\r\n");
            EndContext();
#line 61 "D:\Repository\stax\STAX\Views\Shared\Components\Messages\Default.cshtml"
     if(Model.UserPermissions.MessageAccessLevel == STAX.Models.UserPermissions.MessageAccessLevels.Read_And_Send)
    {

#line default
#line hidden
            BeginContext(2893, 168, true);
            WriteLiteral("        <!--Gated access for message composition-->\r\n        <div class=\"modal_option\" js-paneVal=\"1\" onclick=\"SwitchPane(this)\" title=\"Write a Message\">Compose</div>\r\n");
            EndContext();
#line 65 "D:\Repository\stax\STAX\Views\Shared\Components\Messages\Default.cshtml"
    }

#line default
#line hidden
            BeginContext(3068, 6, true);
            WriteLiteral("</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<STAX.Models.User> Html { get; private set; }
    }
}
#pragma warning restore 1591
