﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace STAX.Migrations
{
    public partial class SpecUpdateItemAmend : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NoteType",
                table: "SpecUpdateItem");

            migrationBuilder.AddColumn<string>(
                name: "ChangeLog",
                table: "SpecUpdateItem",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChangeLog",
                table: "SpecUpdateItem");

            migrationBuilder.AddColumn<int>(
                name: "NoteType",
                table: "SpecUpdateItem",
                nullable: false,
                defaultValue: 0);
        }
    }
}
