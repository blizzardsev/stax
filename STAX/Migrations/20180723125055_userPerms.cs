﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace STAX.Migrations
{
    public partial class userPerms : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserPermissionsID",
                table: "User",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "UserPermissions",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreateSpecLevel = table.Column<int>(nullable: false),
                    EditSpecLevel = table.Column<int>(nullable: false),
                    SpecialAccessLevel = table.Column<int>(nullable: false),
                    MessageAccessLevel = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserPermissions", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_User_UserPermissionsID",
                table: "User",
                column: "UserPermissionsID");

            migrationBuilder.AddForeignKey(
                name: "FK_User_UserPermissions_UserPermissionsID",
                table: "User",
                column: "UserPermissionsID",
                principalTable: "UserPermissions",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_User_UserPermissions_UserPermissionsID",
                table: "User");

            migrationBuilder.DropTable(
                name: "UserPermissions");

            migrationBuilder.DropIndex(
                name: "IX_User_UserPermissionsID",
                table: "User");

            migrationBuilder.DropColumn(
                name: "UserPermissionsID",
                table: "User");

            migrationBuilder.DropColumn(
                name: "SpecNotes",
                table: "SpecMaster");

            migrationBuilder.CreateTable(
                name: "SpecNote",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Content = table.Column<string>(nullable: true),
                    SpecMasterID = table.Column<int>(nullable: true),
                    UserID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SpecNote", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SpecNote_SpecMaster_SpecMasterID",
                        column: x => x.SpecMasterID,
                        principalTable: "SpecMaster",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SpecNote_User_UserID",
                        column: x => x.UserID,
                        principalTable: "User",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SpecNote_SpecMasterID",
                table: "SpecNote",
                column: "SpecMasterID");

            migrationBuilder.CreateIndex(
                name: "IX_SpecNote_UserID",
                table: "SpecNote",
                column: "UserID");
        }
    }
}
