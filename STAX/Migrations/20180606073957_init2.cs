﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace STAX.Migrations
{
    public partial class init2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UserPreferences",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SiteAnimations = table.Column<bool>(nullable: false),
                    DisableMessages = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserPreferences", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    WindowsDirRef = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    SecondName = table.Column<string>(nullable: true),
                    ProfileImageLoc = table.Column<string>(nullable: true),
                    TeamName = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    LastActive = table.Column<DateTime>(nullable: false),
                    UserPreferencesID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.ID);
                    table.ForeignKey(
                        name: "FK_User_UserPreferences_UserPreferencesID",
                        column: x => x.UserPreferencesID,
                        principalTable: "UserPreferences",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Message",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MessageTitle = table.Column<string>(nullable: true),
                    MessageContent = table.Column<string>(nullable: true),
                    MessageExpires = table.Column<bool>(nullable: false),
                    MessageSent = table.Column<DateTime>(nullable: false),
                    MessageVisible = table.Column<bool>(nullable: false),
                    SystemMessage = table.Column<bool>(nullable: false),
                    SenderUserID = table.Column<int>(nullable: true),
                    ReceiverUserID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Message", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Message_User_ReceiverUserID",
                        column: x => x.ReceiverUserID,
                        principalTable: "User",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Message_User_SenderUserID",
                        column: x => x.SenderUserID,
                        principalTable: "User",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SpecMaster",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CrNumber = table.Column<string>(nullable: true),
                    SpecName = table.Column<string>(nullable: true),
                    SpecDesc = table.Column<string>(nullable: true),
                    CreatorUserID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SpecMaster", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SpecMaster_User_CreatorUserID",
                        column: x => x.CreatorUserID,
                        principalTable: "User",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SpecNote",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Content = table.Column<string>(nullable: true),
                    SpecMasterID = table.Column<int>(nullable: true),
                    UserID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SpecNote", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SpecNote_SpecMaster_SpecMasterID",
                        column: x => x.SpecMasterID,
                        principalTable: "SpecMaster",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SpecNote_User_UserID",
                        column: x => x.UserID,
                        principalTable: "User",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SpecRtpItem",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ItemOrderNumber = table.Column<int>(nullable: false),
                    ItemType = table.Column<int>(nullable: false),
                    ItemText = table.Column<string>(nullable: true),
                    ItemOption = table.Column<int>(nullable: false),
                    UserID = table.Column<int>(nullable: true),
                    SpecMasterID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SpecRtpItem", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SpecRtpItem_SpecMaster_SpecMasterID",
                        column: x => x.SpecMasterID,
                        principalTable: "SpecMaster",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SpecRtpItem_User_UserID",
                        column: x => x.UserID,
                        principalTable: "User",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Message_ReceiverUserID",
                table: "Message",
                column: "ReceiverUserID");

            migrationBuilder.CreateIndex(
                name: "IX_Message_SenderUserID",
                table: "Message",
                column: "SenderUserID");

            migrationBuilder.CreateIndex(
                name: "IX_SpecMaster_CreatorUserID",
                table: "SpecMaster",
                column: "CreatorUserID");

            migrationBuilder.CreateIndex(
                name: "IX_SpecNote_SpecMasterID",
                table: "SpecNote",
                column: "SpecMasterID");

            migrationBuilder.CreateIndex(
                name: "IX_SpecNote_UserID",
                table: "SpecNote",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_SpecRtpItem_SpecMasterID",
                table: "SpecRtpItem",
                column: "SpecMasterID");

            migrationBuilder.CreateIndex(
                name: "IX_SpecRtpItem_UserID",
                table: "SpecRtpItem",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_User_UserPreferencesID",
                table: "User",
                column: "UserPreferencesID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Message");

            migrationBuilder.DropTable(
                name: "SpecNote");

            migrationBuilder.DropTable(
                name: "SpecRtpItem");

            migrationBuilder.DropTable(
                name: "SpecMaster");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "UserPreferences");
        }
    }
}
