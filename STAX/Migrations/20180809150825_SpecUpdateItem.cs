﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace STAX.Migrations
{
    public partial class SpecUpdateItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SpecNoteItem_SpecMaster_SpecMasterID",
                table: "SpecNoteItem");

            migrationBuilder.DropForeignKey(
                name: "FK_SpecNoteItem_User_UserID",
                table: "SpecNoteItem");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SpecNoteItem",
                table: "SpecNoteItem");

            migrationBuilder.RenameTable(
                name: "SpecNoteItem",
                newName: "SpecUpdateItem");

            migrationBuilder.RenameIndex(
                name: "IX_SpecNoteItem_UserID",
                table: "SpecUpdateItem",
                newName: "IX_SpecUpdateItem_UserID");

            migrationBuilder.RenameIndex(
                name: "IX_SpecNoteItem_SpecMasterID",
                table: "SpecUpdateItem",
                newName: "IX_SpecUpdateItem_SpecMasterID");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationDate",
                table: "SpecUpdateItem",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddPrimaryKey(
                name: "PK_SpecUpdateItem",
                table: "SpecUpdateItem",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_SpecUpdateItem_SpecMaster_SpecMasterID",
                table: "SpecUpdateItem",
                column: "SpecMasterID",
                principalTable: "SpecMaster",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SpecUpdateItem_User_UserID",
                table: "SpecUpdateItem",
                column: "UserID",
                principalTable: "User",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SpecUpdateItem_SpecMaster_SpecMasterID",
                table: "SpecUpdateItem");

            migrationBuilder.DropForeignKey(
                name: "FK_SpecUpdateItem_User_UserID",
                table: "SpecUpdateItem");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SpecUpdateItem",
                table: "SpecUpdateItem");

            migrationBuilder.DropColumn(
                name: "CreationDate",
                table: "SpecUpdateItem");

            migrationBuilder.RenameTable(
                name: "SpecUpdateItem",
                newName: "SpecNoteItem");

            migrationBuilder.RenameIndex(
                name: "IX_SpecUpdateItem_UserID",
                table: "SpecNoteItem",
                newName: "IX_SpecNoteItem_UserID");

            migrationBuilder.RenameIndex(
                name: "IX_SpecUpdateItem_SpecMasterID",
                table: "SpecNoteItem",
                newName: "IX_SpecNoteItem_SpecMasterID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SpecNoteItem",
                table: "SpecNoteItem",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_SpecNoteItem_SpecMaster_SpecMasterID",
                table: "SpecNoteItem",
                column: "SpecMasterID",
                principalTable: "SpecMaster",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SpecNoteItem_User_UserID",
                table: "SpecNoteItem",
                column: "UserID",
                principalTable: "User",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
