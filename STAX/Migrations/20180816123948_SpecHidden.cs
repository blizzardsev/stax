﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace STAX.Migrations
{
    public partial class SpecHidden : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "SpecHidden",
                table: "SpecMaster",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SpecHidden",
                table: "SpecMaster");
        }
    }
}
