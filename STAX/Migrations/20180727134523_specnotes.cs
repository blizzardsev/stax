﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace STAX.Migrations
{
    public partial class specnotes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SpecStatus",
                table: "SpecMaster",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "SpecNoteItem",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Content = table.Column<string>(nullable: true),
                    NoteType = table.Column<int>(nullable: false),
                    UserID = table.Column<int>(nullable: true),
                    SpecMasterID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SpecNoteItem", x => x.ID);
                    table.ForeignKey(
                        name: "FK_SpecNoteItem_SpecMaster_SpecMasterID",
                        column: x => x.SpecMasterID,
                        principalTable: "SpecMaster",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SpecNoteItem_User_UserID",
                        column: x => x.UserID,
                        principalTable: "User",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SpecNoteItem_SpecMasterID",
                table: "SpecNoteItem",
                column: "SpecMasterID");

            migrationBuilder.CreateIndex(
                name: "IX_SpecNoteItem_UserID",
                table: "SpecNoteItem",
                column: "UserID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SpecNoteItem");

            migrationBuilder.DropColumn(
                name: "SpecStatus",
                table: "SpecMaster");
        }
    }
}
