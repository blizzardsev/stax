﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace STAX.ViewComponents
{
    public class ConfirmPopUpViewComponent : ViewComponent
    {
        public ConfirmPopUpViewComponent()
        {

        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View();
        }
    }
}
