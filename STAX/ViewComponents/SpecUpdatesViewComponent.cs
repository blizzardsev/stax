﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using STAX.Data;
using STAX.Models;

namespace STAX.ViewComponents
{
    public class SpecUpdatesViewComponent : ViewComponent
    {
        private readonly StaxContext _context;

        //Constructor
        public SpecUpdatesViewComponent(StaxContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Provide data for spec updates modal.
        /// </summary>
        /// <returns></returns>
        public async Task<IViewComponentResult> InvokeAsync(int specMasterID)
        {
            return View(await _context.SpecMasterCollection
                .Include(specMaster => specMaster.SpecUpdateItems)
                .SingleAsync(specMaster => specMaster.ID == specMasterID));
        }
    }
}
