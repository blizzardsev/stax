﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using STAX.Data;
using STAX.Models;

namespace STAX.ViewComponents
{
    public class CreateSpecViewComponent : ViewComponent
    {
        private readonly StaxContext _context;

        //Constructor
        public CreateSpecViewComponent(StaxContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Provide data for messages modal.
        /// </summary>
        /// <returns></returns>
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View();
        }
    }
}
