﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using STAX.Data;
using STAX.Models;

namespace STAX.ViewComponents
{
    public class PreferencesViewComponent : ViewComponent
    {
        private readonly StaxContext _context;

        //Constructor
        public PreferencesViewComponent(StaxContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Provide data for prefs modal.
        /// </summary>
        /// <returns></returns>
        public async Task<IViewComponentResult> InvokeAsync()
        {
            User thisUser = await _context.UserCollection
                .Include(user => user.UserPreferences)
                .Include(user => user.UserPermissions)
                .SingleAsync(user => user.WindowsDirRef == User.Identity.Name);
            return View(thisUser);
        }
    }
}
