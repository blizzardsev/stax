﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using STAX.Data;
using STAX.Models;

namespace STAX.ViewComponents
{
    public class SelectItemsViewComponent : ViewComponent
    {
        //Constructor
        public SelectItemsViewComponent()
        {

        }

        /// <summary>
        /// Render options according to type.
        /// </summary>
        /// <returns></returns>
        public async Task<IViewComponentResult> InvokeAsync(int type)
        {
            ViewBag.Type = type;
            return View();
        }
    }
}
