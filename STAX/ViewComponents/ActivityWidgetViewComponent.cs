﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using STAX.Data;
using STAX.Models;

namespace STAX.ViewComponents
{
    public class ActivityWidgetViewComponent : ViewComponent
    {
        private readonly StaxContext _context;

        //Constructor
        public ActivityWidgetViewComponent(StaxContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Provide data for activity widget.
        /// </summary>
        /// <returns></returns>
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View(new ActivityWidgetData(
                await _context.MessageCollection
                    .Include(message => message.SenderUser)
                    .Include(message => message.ReceiverUser)
                    .OrderByDescending(message => message.ID)
                .ToListAsync(),
                await _context.SpecUpdateCollection
                    .Include(specUpdate => specUpdate.User)
                    .Include(specUpdate => specUpdate.SpecMaster)
                    .OrderByDescending(specUpdate => specUpdate.ID)
                .ToListAsync()
                ));
        }
    }
}
