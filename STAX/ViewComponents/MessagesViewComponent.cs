﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using STAX.Data;
using STAX.Models;

namespace STAX.ViewComponents
{
    public class MessagesViewComponent : ViewComponent
    {
        private readonly StaxContext _context;

        //Constructor
        public MessagesViewComponent(StaxContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Provide data for messages modal.
        /// </summary>
        /// <returns></returns>
        public async Task<IViewComponentResult> InvokeAsync()
        {
            ViewBag.AllUsers = await _context.UserCollection.Where(user => user.ID != 1).ToListAsync();
            ViewBag.AllMessages = await _context.MessageCollection
                .Include(message => message.ReceiverUser)
                .Where(message => message.ReceiverUser == _context.UserCollection.Single(user => user.WindowsDirRef == User.Identity.Name))
                .ToListAsync();
            return View(_context.UserCollection
                .Include(user => user.UserPermissions)
                .Single(user => user.WindowsDirRef == User.Identity.Name));
        }
    }
}
