using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using STAX.Models;
using STAX.Data;
using STAX.JSON;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace STAX.Data
{
    public class DataWidgetData
    {
        //Attributes
        public List<SpecMaster> allSpecs;
        public List<User> allUsers;
        public List<Message> allMessages;

        //Constructor
        public DataWidgetData(List<SpecMaster> specs, List<User> users, List<Message> messages)
        {
            this.allSpecs = specs;
            this.allUsers = users;
            this.allMessages = messages;
        }
    }
}
