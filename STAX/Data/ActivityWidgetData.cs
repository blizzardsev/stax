using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using STAX.Models;
using STAX.Data;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace STAX.Data
{
    public class ActivityWidgetData
    {
        //Attributes
        public List<Message> allMessages;
        public List<SpecUpdateItem> allSpecUpdates;

        //Constrictor
        public ActivityWidgetData(List<Message> messages, List<SpecUpdateItem> updates)
        {
            this.allMessages = messages;
            this.allSpecUpdates = updates;
        }
    }
}
