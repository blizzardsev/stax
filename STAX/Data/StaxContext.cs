﻿using Microsoft.EntityFrameworkCore;
using STAX.Models;

namespace STAX.Data
{
    //EF setup for STAX
    public class StaxContext : DbContext
    {
        public DbSet<SpecMaster> SpecMasterCollection { get; set; }
        public DbSet<SpecRtpItem> SpecRtpItemCollection { get; set; }
        public DbSet<Message> MessageCollection { get; set; }
        public DbSet<User> UserCollection { get; set; }
        public DbSet<UserPreferences> UserPrefsCollection { get; set; }
        public DbSet<UserPermissions> UserPermsCollection { get; set; }
        public DbSet<SpecUpdateItem> SpecUpdateCollection { get; set; }

        //Constructor
        public StaxContext(DbContextOptions options) : base(options)
        {
			
        }

        //Table creation
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SpecMaster>().ToTable("SpecMaster");
            modelBuilder.Entity<SpecRtpItem>().ToTable("SpecRtpItem");
            modelBuilder.Entity<Message>().ToTable("Message");
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<UserPreferences>().ToTable("UserPreferences");
            modelBuilder.Entity<UserPermissions>().ToTable("UserPermissions");
            modelBuilder.Entity<SpecUpdateItem>().ToTable("SpecUpdateItem");
        }
    }
}
