﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using MailKit;

namespace STAX_LIB
{
    /// <summary>
    /// Handle email creation and distribution
    /// </summary>
    public class EmailFactory
    {
        //Attributes
        private String loginAddress { get; set; }
        private String loginPass { get; set; }

        //Constructor
        public EmailFactory()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailTemplate"></param>
        /// <param name="emailContent"></param>
        /// <param name="emailTargets"></param>
        public void CreateEmail(
            String emailContent,
            Dictionary<String, String> emailPairs,
            List<EmailTarget> emailTargets)
        {
            //Replace the main content
            emailContent.Replace("[emailContent]", emailContent);

            //Replace tagged items with given values within main content block
            for (int i = 0; i < emailPairs.Count; i++)
            {
                emailContent.Replace(emailPairs.Keys.ElementAt(i), emailPairs.Values.ElementAt(i));
            }

            //Replace the signature
            emailContent.Replace("[emailSignature]", emailContent);
        }

        /// <summary>
        /// 
        /// </summary>
        public void SendEmails()
        {

        }
    }
}
