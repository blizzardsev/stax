﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STAX_LIB
{
    //Wrapper for targeted email addresses
    public class EmailTarget
    {
        public String targetSalutation { get; set; }
        public String targetEmailAddress { get; set; }

        public List<Object> targetAuxData { get; set; }
    }
}
